
const moment = require("moment");
const axios = require("axios");
const async = require("async");
const _ = require("lodash")
const getFitBitData = require('./trashCode/getFitBitData'); 
const { DATE_KEY, DATE_FORMAT,ACTIVITY_ID_KEY,ACTIVITY_ID_DATE_TIME_FORMAT } = require("./constants");
const googleFitMapping = require("./activityMappers/googleFitMapping");
const activityMasterData = require("./activityMappers/activityMasterData");

exports.updateActivityData = async function (req, res, db) {

    console.log("Request",req.body)
    let userID = req.body.userID 
    let token = req.headers.authorization
    let PrimarySourceID = req.body.sourceID || "1"
    let PrimarySourceName = req.body.primarySourceName || "GoogleFit"
    let totalNumberOfActivities = 0;
   // const endDate = req.body.endDate
   // endDate = currentDate

    console.log("token:  ", token)
    let apiEndDate = moment()
    let apiStartDate;
    let apiActivities = []
    let calendarToSet = []
    let callback = {}
    try {
        let userRef = db.collection("users");
        let calendarRef = db.collection("calendar")
        let activitiesRef = db.collection("activities")
        let userDetails = await userRef.where("UserID", "==", userID).get()
        
        let lastActivityUpdated 
        let weeklyFitScore = 0;
        if (userDetails.empty) {
            //No data is present for the user
            //Pull data from start of week before requested date
            
            apiStartDate =  moment().subtract(7,"day").startOf('week');
        } else {
            //Data is present for the user
            userDetails.forEach(doc => {
                lastActivityUpdated = moment(doc.data().LastActivityUpdated)
                totalNumberOfActivities = doc.data().TotalNumberOfActivities
            });
            //Get data from one day before lastActivityUpdated
             apiStartDate = moment(lastActivityUpdated).subtract(1,"day").startOf('day')
             console.log("Api Start Date", apiStartDate)
        }
         
        if (PrimarySourceID == 1) {
            // let googleAPIRequest = {
            //     totalNumberOfActivities, apiStartDate, apiEndDate ,token 
            // }
            // let googleAPIResponse = await getGoogleFitData(googleAPIRequest)
            // totalNumberOfActivities = googleAPIResponse.totalNumberOfActivities
            // apiActivities = googleAPIResponse.activitiesFromAPI\

            // google fit data
        //Get steps data
        var stepData = {
            "aggregateBy": [{
                "dataTypeName": "com.google.step_count.delta",
                "dataSourceId": "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps"
            }],
            "bucketByTime": { "durationMillis": 86400000 },
            "startTimeMillis": moment(apiStartDate, DATE_FORMAT).valueOf(),
            "endTimeMillis": moment(apiEndDate, DATE_FORMAT).valueOf()

        }
        console.log("Body for second api ", stepData)
        await axios.post('https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate',stepData, {
            headers: {
                "Authorization" : token
            }    
        })
        .then(async function (response){
            var result = response.data
            console.log("step data", result.bucket)
            if (result && result.bucket && result.bucket.length > 0) {
                for (let i=0;i<result.bucket.length;i++) {
                    if (result.bucket[i].dataset && result.bucket[i].dataset.length > 0 && result.bucket[i].dataset[0].point && result.bucket[i].dataset[0].point.length > 0) {
                        let obj = {}
                        let activityID = userID + "_" +  moment(Number(result.bucket[i].startTimeMillis)).format(ACTIVITY_ID_DATE_TIME_FORMAT)
                        obj.ActivityID = activityID
                        obj.ActivityName = "Daily Steps"
                            
                        obj.StartDate = moment(Number(result.bucket[i].startTimeMillis)).startOf('day').valueOf()
                        obj.EndDate = moment(Number(result.bucket[i].endTimeMillis)).startOf('day').valueOf()
                        //obj.ActivityID = 100
                        if (result.bucket[i].dataset[0].point[0].value && result.bucket[i].dataset[0].point[0].value.length > 0) {
                            obj.StepCount = result.bucket[i].dataset[0].point[0].value[0].intVal
                            obj.StepFitPoints = calculateStepFitPoints(obj.StepCount)
                        }
                        if(lastActivityUpdated && moment(obj.StartDate).isSameOrBefore(lastActivityUpdated)){
                            let exisingActivity = await activitiesRef.where(ACTIVITY_ID_KEY,"==", activityID).get();
                            if(exisingActivity && exisingActivity.length > 0){
                               // if already exist do not increase activity count
                            } else{                        
                                totalNumberOfActivities ++ 
                            }
                        } else {
                            totalNumberOfActivities ++
                        }
                        apiActivities.push(obj)
                    }
                }
            }
        })
        .catch(function (error){
            console.log("error in second api ", error)
            apiResponse = {
                success: false,
                status: error.code,
                error: error,
                request: data,
            }
        })  
        //Get Activities data   
        var data = {
            "aggregateBy": 
            [
                {
                    "dataTypeName": "com.google.step_count.delta",
                },
                {
                    "dataTypeName": "com.google.calories.expended",
                },
                {
                    "dataTypeName": "com.google.heart_minutes",
                },
                {
                    "dataTypeName": "com.google.distance.delta"
                },
            ],
            "bucketByActivitySegment": {
                "minDurationMillis": 180000
            },
            "startTimeMillis": moment(apiStartDate,).valueOf(),
            "endTimeMillis": moment(apiEndDate).valueOf()
        }
        await axios.post('https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate',data, {
            headers: {  
                "Authorization" : token
            }    
        
        })
        .then(async function (response) {
            // handle success
            console.log("Got response from google fit",response)
            let result = response.data
            if (result.bucket && result.bucket.length) {
                for (let i =0;i<result.bucket.length;i++) {
                    let obj = {}
                    //ActivityID will be combination of UserID and timestamp
                    let activityID = userID + "_" +  moment(Number(result.bucket[i].startTimeMillis)).format(ACTIVITY_ID_DATE_TIME_FORMAT)
                    obj.ActivityID = activityID
                    let activityName =  googleFitMapping[result.bucket[i].activity]
                    
                    console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
                    console.log(activityName)
                    console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

                    if(activityName ) {
                        let activityConfig = activityMasterData[activityName]
                        let metValue = activityConfig.METsValue;
                        obj.ActivityName = activityName

                        obj.StartDate = Number(result.bucket[i].startTimeMillis)
                        obj.EndDate = Number(result.bucket[i].endTimeMillis)
                        let activityDuration = Math.round(moment.duration(obj.EndDate - obj.StartDate).asMinutes())

                        if (result.bucket[i].dataset && result.bucket[i].dataset.length > 0) {
                            for (let j=0;j<result.bucket[i].dataset.length;j++) {
                                if (result.bucket[i].dataset[j].point && result.bucket[i].dataset[j].point.length > 0) {
                                    if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.calories.expended:com.google.android.gms:aggregated") {
                                    // count calories 
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.CaloriesBurned = Number(result.bucket[i].dataset[j].point[0].value[0].fpVal).toFixed(2)  
                                        }
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.heart_minutes.summary:com.google.android.gms:aggregated") {
                                        // count heart points 
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.HeartPoints = Number(result.bucket[i].dataset[j].point[0].value[0].fpVal).toFixed(2)
                                       // console.log("Duration ", result.bucket[i].dataset[j].point[0].value[1])
                                          //  obj.Duration = result.bucket[i].dataset[j].point[0].value[1].intVal
                                           obj.Duration = activityDuration
                                           obj.ActivityScore = calculateActivityScore(obj.HeartPoints, obj.Duration, metValue)
                                            }
                                        
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.step_count.delta:com.google.android.gms:aggregated") {
                                    // count steps
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.StepCount = result.bucket[i].dataset[j].point[0].value[0].intVal
                                            obj.StepFitPoints = calculateStepFitPoints(obj.StepCount)
                                        }
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.distance.delta:com.google.android.gms:aggregated") {
                                       
                                        // count distance and average pace
                                        //TODO : min/km for walking and running, km/hr for cycling
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.Distance = Number(result.bucket[i].dataset[j].point[0].value[0].fpVal/1000).toFixed(2)
                                       
                                        
                                            if(obj.Duration){
                                                if(activityName == "Walking" || activityName == "Running"){
                                                //min/km
                                                obj.AveragePace = Number(obj.Duration/obj.Distance).toFixed(2)
                                                } else {
                                                //For other distance based activities : km/hr
                                                obj.AveragePace = Number(obj.Distance/obj.Duration).toFixed(2) * 60 
                                                }
                                            }   
                                        }  
                                    }
                                    //get The source of activity 
                                    let source = result.bucket[i].dataset[j].point[0].originDataSourceId 
                                    console.log("------------------------------------------------")
                                    console.log("source = ", so)
                                    console.log("-------------------------------------------------")
                                    
                                }   
                            }    
                        }
                        if(activityName == "Walking" || activityName == "Running"){

                           obj.ActivityScore = obj.AveragePace < 8.50 ? (2* obj.Duration) : obj.Duration

                        }
                        //If heart points are not given
                        if(!obj.ActivityScore){
                           
                            obj.ActivityScore = calculateActivityScore(0, activityDuration, metValue)
                        }     
                        if(lastActivityUpdated && moment(obj.StartDate).isSameOrBefore(lastActivityUpdated)) {
                            let exisingActivity = await activitiesRef.where("ActivityID","==", activityID).get();
                            if(exisingActivity && exisingActivity.length > 0){
                                 // if already exist do not increase activity count
                            } else{                        
                            totalNumberOfActivities++ 
                            }
                        } else { 
                        totalNumberOfActivities++ 
                        }
                        if(activityName == "Walking" || activityName == "Running") {
                           if(obj.Distance >= 1){
                            apiActivities.push(obj)
                           } else {
                            totalNumberOfActivities -- 
                           }
                        }else {
                        apiActivities.push(obj)
                        }
                     
                    } else {
                        console.log("Invalid activity")
                    }
                }
            } else {
                // no data
                console.log("no data")
            }

        })
        .catch(function (error) {
            // handle error
            console.log("Error Excpetion api 1 ", error)
            apiResponse = {
                success: false,
                status: error.code,
                error: error,
                request: data,
            }
        })
        } else if (PrimarySourceID == 2) {
            // update fitbit data
            let userExists = false;
            var sourceUserID = req.body.sourceUserID
            if(!userDetails.empty) {
                userExists = true;
            }
            let requestUrl = 'https://api.fitbit.com/1/user/' + sourceUserID + '/activities/list.json?';
    //         let requestUrl = 'https://api.fitbit.com/1/user/' + "-";
            if(!userExists) {
                requestUrl = requestUrl + 'beforeDate=' + moment().format('YYYY-MM-DD');
            } else {
                requestUrl = requestUrl + 'afterDate=' + moment(userDetails.LastActivityUpdated).format('YYYY-MM-DD');
            }
            requestUrl = requestUrl + '&sort=desc&limit=20&offset=0';
            console.log("Reached here 1")
            await axios.get(requestUrl,{
                headers: {
                    Authorization:token
                }
            })
            .then(async function (response){
                var result = response.data
                if (result.activities && result.activities.length > 0) {
                    console.log("length ", result.activities.length)
                    for (var i=0;i<result.activities.length;i++) {
                        var obj = {}
                        let activityID = userID + "_" +  moment(Number(result.bucket[i].startTimeMillis)).format(ACTIVITY_ID_DATE_TIME_FORMAT)
                        obj.ActivityID =activityID
                        obj.ActivityName = result.activities[i].activityName
                        obj.Duration = result.activities[i].activeDuration/60000
                        obj.CaloriesBurned = Number(result.activities[i].calories).toFixed(2)
                        obj.StepCount = result.activities[i].steps
                        obj.Distance = result.activities[i].distance
                        obj.AveragePace = result.activities[i].speed
                        obj.HeartPoints = result.activities[i].averageHeartRate
                        if (obj.Duration) {
                            obj.ActivityScore = calculateActivityScore(obj.HeartPoints, obj.Duration)
                        }
                        obj.StepFitPoints = calculateStepFitPoints(obj.StepCount)
                        obj.StartDate = moment(result.activities[i].startTime, "YYYY-MM-DD").startOf('day').format("MM-DD-YYYY hh:mm:ss")
                        
                        if(lastActivityUpdated && moment(obj.StartDate).isSameOrBefore(lastActivityUpdated)){
                            let exisingActivity = await activitiesRef.where("ActivityID","==", activityID).get();
                            if(exisingActivity && exisingActivity.length > 0){
                                // if already exist do not increase activity count
                            } else{                        
                                totalNumberOfActivities++ 
                            }
                        }
                        apiActivities.push(obj)
                    }
                }
                
            })
            .catch(function (error){
                console.log("error in second api ", error)
                callback = {
                    success: false,
                    status: 400,
                    error: error,
                    request: data,
                }
            })
        }
        
        //New integrations

         //Update Activity Collection
         for (let i=0;i<apiActivities.length;i++) {
                
            await activitiesRef.doc(apiActivities[i].ActivityID).set({
                UserID: userID, 
                ActivityName: apiActivities[i].ActivityName, 
                ActivityID: apiActivities[i].ActivityID, 
                StartDate: apiActivities[i].StartDate ? apiActivities[i].StartDate : undefined,
                EndDate: apiActivities[i].EndDate ? apiActivities[i].EndDate : undefined,
                Duration: apiActivities[i].Duration ? apiActivities[i].Duration : 0,
                CaloriesBurned: apiActivities[i].CaloriesBurned ? Number(apiActivities[i].CaloriesBurned).toFixed(2) : 0,
                ActivityScore: apiActivities[i].ActivityScore ? Number(apiActivities[i].ActivityScore).toFixed(2) : 0,
                StepCount: apiActivities[i].StepCount ? apiActivities[i].StepCount: 0,
                StepFitPoints: apiActivities[i].StepFitPoints ? Number(apiActivities[i].StepFitPoints).toFixed(2) : 0,
                Distance: apiActivities[i].Distance ? Number(apiActivities[i].Distance).toFixed(2) : 0,
                AveragePace: apiActivities[i].AveragePace ? Number(apiActivities[i].AveragePace).toFixed(2) : 0
                
            });
        }
       
        //Update Calendar Collection
        let activityByDate = []
        activityByDate = _.groupBy(apiActivities, function (o) {
            return moment(o.StartDate).format(DATE_FORMAT)
        })
        for (let key in activityByDate) {
            console.log("===========================saving update===============================================================")
            console.log("key ", key )
            console.log("==========================================================================================")
            let obj = {
                Date : key,                
                UserID: userID
            }
            let dailyFitScore = 0
            let activityIDs = []
                for (let i=0;i<activityByDate[key].length;i++) {
                    
                    if(!isNaN(activityByDate[key][i].ActivityScore)){
                        // for All other activity
                        dailyFitScore = activityByDate[key][i].ActivityScore + dailyFitScore
                    }
                    else {
                        //For Daily Steps activity
                        if (!isNaN(activityByDate[key][i].StepFitPoints)) {
                            dailyFitScore = activityByDate[key][i].StepFitPoints + dailyFitScore
                        }
                    }
                    activityIDs.push(activityByDate[key][i].ActivityID)
                }
            obj.DailyFitScore = Number(dailyFitScore).toFixed(2)
            obj.ActivityIDs = activityIDs
            calendarToSet.push(obj)
        }
      //Set calendar 
        for (let i =0;i<calendarToSet.length;i++) {
            await calendarRef.doc(calendarToSet[i].Date+"_"+calendarToSet[i].UserID).set({
                Date: calendarToSet[i].Date,
                UserID: calendarToSet[i].UserID,
                ActivityIDs: calendarToSet[i].ActivityIDs,
                DailyFitScore: calendarToSet[i].DailyFitScore
            });            
        }
        
        //Calculate weekly fit score 
        let weeklyCalendarData = await calendarRef.where(DATE_KEY,">=", moment().startOf('week').format(DATE_FORMAT)).get();
       
        weeklyCalendarData.forEach(doc => {
            console.log("))))))))))))))))))))))))))))))))))))))))))")
            console.log(doc.data().DailyFitScore)
            weeklyFitScore = Number(doc.data().DailyFitScore) + weeklyFitScore;
        });

        //Update User Collection
        if (userDetails.empty) {
            await userRef.doc(userID.toString()).set({
                UserID: userID,
                PrimarySourceID: PrimarySourceID, 
                PrimarySourceName: PrimarySourceName,
                LastActivityUpdated: moment().valueOf(),
                AccessToken: token,
                TotalNumberOfActivities: totalNumberOfActivities,
                WeeklyFitScore: weeklyFitScore
            });
        } else {
            await userRef.doc(userID.toString()).update({
                LastActivityUpdated : moment().valueOf(),
                TotalNumberOfActivities: totalNumberOfActivities,
                WeeklyFitScore: weeklyFitScore
            });
        }

        callback = {
            data: "Data updated successfully",
            status: 200,
            result: true
        }
    } catch (e) {
        console.log("Exception 2 ", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }
    return callback
};

var calculateStepFitPoints = function (steps) {
    return Math.round(steps * 0.002)
    // var score = 0;
    // if (steps < 2500) {
    //     score = 0 
    // } else if (steps >=2500 && steps < 5000) {
    //     score = 0.6 * ((steps - 2500)/ 500)
    // } else if (steps >= 5000 && steps < 7500) {
    //   score = 0.6 * ((steps - 2500)/ 500)
    // } else if (steps >= 7500 && steps < 10000) {
    //     score = 0.6 * 30
    // } else if (steps >= 10000 && steps < 12500) {
    //     score = 0.6 * (30 + ((steps - 10000)/ 500))
    // } else {
    //   score = 0.6 * (35 + ((steps - 12500)/ 500))
    // }
    // return score
}

var calculateActivityScore = function (heartPoints, duration, metValue) {
    var activityScore = 0
    if (heartPoints > duration) {
        activityScore = heartPoints
    } else {
        if(metValue<6){
            activityScore = duration
        } else {
            activityScore = 2*duration
        }
    }
    return activityScore
}

var getGoogleFitData = async function(apiRequest) {
    console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++")
        console.log(apiRequest)
        console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++")
        let totalNumberOfActivities = apiRequest.totalNumberOfActivities
        let apiStartDate = apiRequest.startDate
        let apiEndDate = apiRequest.endDate
        let token = apiRequest.token
        let apiResponse = {}
    
        let apiActivities = [];
         
    
        apiResponse = {
            totalNumberOfActivities : totalNumberOfActivities,
            activitiesFromAPI : apiActivities
        }
        return apiResponse;
        
    };