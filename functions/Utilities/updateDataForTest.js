const moment = require("./node_modules/moment");
const axios = require("./node_modules/axios");
const async = require("./node_modules/async");
const _ = require("./node_modules/lodash")
//const admin = require('firebase-admin');
//const db = admin.firestore();
exports.updateDataForTest = async function (req, res, db) {

    const userRef = db.collection('users');
    const calendarRef = db.collection("calendar")
    const activitiesRef = db.collection("activities")
    const response= {};

let users = [{
    "LastActivityUpdated" : "6 Feburary 201 at node  UTC+5:30",
    "UserID": "1",
    "PrimarySourceId":"1",
    "PrimarySourceName" : "GoogleFit", 
    "TotalNumberOfActivities" : 8,
    "WeeklyFitScore": 60
},
{
    "LastActivityUpdated" : "6 Feburary 201 at 16:50:43 UTC+5:30",
    "UserID": "2",
    "PrimarySourceId":"1",
    "PrimarySourceName" : "GoogleFit", 
    "TotalNumberOfActivities" : 8,
    "WeeklyFitScore": 40
},
{
    "LastActivityUpdated" : "6 Feburary 201 at 16:50:43 UTC+5:30",
    "UserID": "3",
    "PrimarySourceId":"1",
    "PrimarySourceName" : "GoogleFit", 
    "TotalNumberOfActivities" : 8,
    "WeeklyFitScore": 990
},
{
    "LastActivityUpdated" : "6 Feburary 201 at 16:50:43 UTC+5:30",
    "UserID": "3",
    "PrimarySourceId":"1",
    "PrimarySourceName" : "GoogleFit", 
    "TotalNumberOfActivities" : 8,
    "WeeklyFitScore": 640
},
{
    "LastActivityUpdated" : "6 Feburary 201 at 16:50:43 UTC+5:30",
    "UserID": "4",
    "PrimarySourceId":"1",
    "PrimarySourceName" : "GoogleFit", 
    "TotalNumberOfActivities" : 8,
    "WeeklyFitScore": 620
},
{
    "LastActivityUpdated" : "6 Feburary 201 at 16:50:43 UTC+5:30",
    "UserID": "5",
    "PrimarySourceId":"1",
    "PrimarySourceName" : "GoogleFit", 
    "TotalNumberOfActivities" : 8,
    "WeeklyFitScore": 610
},
{
    "LastActivityUpdated" : "6 Feburary 201 at 16:50:43 UTC+5:30",
    "UserID": "6",
    "PrimarySourceId":"1",
    "PrimarySourceName" : "GoogleFit", 
    "TotalNumberOfActivities" : 8,
    "WeeklyFitScore": 640
},
{
    "LastActivityUpdated" : "6 Feburary 201 at 16:50:43 UTC+5:30",
    "UserID": "7",
    "PrimarySourceId":"1",
    "PrimarySourceName" : "GoogleFit", 
    "TotalNumberOfActivities" : 8,
    "WeeklyFitScore": 670
}
];

let activites= [{"UserID": "1", 
"ActivityName": "ActivityName1",
"ActivityID": "1_1",                
"StartDate": moment().startOf('day'),
"EndDate": "",
"Duration": 1,
"CaloriesBurned": 1,
"HeartPoints": 1,
"ActivityScore": 1,
"StepCount": 1,
"StepFitPoints": 1,
"Distance": 1,
"AveragePace": 1

},
{"UserID": "1", 
"ActivityName": "ActivityName2",
"ActivityID": "1_2",                
"StartDate": moment().startOf('day'),
"EndDate": "",
"Duration": 2,
"CaloriesBurned": 2,
"HeartPoints": 2,
"ActivityScore": 2,
"StepCount": 2,
"StepFitPoints": 2,
"Distance": 2,
"AveragePace": 2

},
{"UserID": "1", 
"ActivityName": "ActivityName3",
"ActivityID": "1_3",                
"StartDate": moment().startOf('week'),
"EndDate": "",
"Duration": 3,
"CaloriesBurned": 3,
"HeartPoints": 3,
"ActivityScore": 3,
"StepCount": 3,
"StepFitPoints": 3,
"Distance": 3,
"AveragePace": 3

},
{"UserID": "1", 
"ActivityName": "ActivityName4",
"ActivityID": "1_4",                
"StartDate": moment().startOf('month'),
"EndDate": "",
"Duration": 4,
"CaloriesBurned": 4,
"HeartPoints": 4,
"ActivityScore": 4,
"StepCount": 4,
"StepFitPoints": 4,
"Distance": 4,
"AveragePace": 4

},
{"UserID": "1", 
"ActivityName": "ActivityName1",
"ActivityID": "1_5",                
"StartDate": moment().startOf('year'),
"EndDate": "",
"Duration": 5,
"CaloriesBurned": 5,
"HeartPoints": 5,
"ActivityScore": 5,
"StepCount": 5,
"StepFitPoints": 5,
"Distance": 5,
"AveragePace": 5

},
{"UserID": "2", 
"ActivityName": "ActivityName1",
"ActivityID": "2_1",                
"StartDate": moment().startOf('month'),
"EndDate": "",
"Duration": 36,
"CaloriesBurned": 33,
"HeartPoints": 86,
"ActivityScore": 345,
"StepCount": 567,
"StepFitPoints": 345,
"Distance": 3456,
"AveragePace": 4567

},
{"UserID": "2", 
"ActivityName": "ActivityName1",
"ActivityID": "2_2",                
"StartDate": "",
"EndDate": "",
"Duration": 36,
"CaloriesBurned": 33,
"HeartPoints": 86,
"ActivityScore": 345,
"StepCount": 567,
"StepFitPoints": 345,
"Distance": 3456,
"AveragePace": 4567

},
{"UserID": "3", 
"ActivityName": "ActivityName1",
"ActivityID": "3_1",                
"StartDate": moment().startOf('week'),
"EndDate": "",
"Duration": 36,
"CaloriesBurned": 33,
"HeartPoints": 86,
"ActivityScore": 345,
"StepCount": 567,
"StepFitPoints": 345,
"Distance": 3456,
"AveragePace": 4567

},
{"UserID": "4", 
"ActivityName": "ActivityName1",
"ActivityID": "4_1",                
"StartDate": "",
"EndDate": "",
"Duration": 36,
"CaloriesBurned": 33,
"HeartPoints": 86,
"ActivityScore": 345,
"StepCount": 567,
"StepFitPoints": 345,
"Distance": 3456,
"AveragePace": 4567

},
{"UserID": "5", 
"ActivityName": "ActivityName1",
"ActivityID": "5_1",                
"StartDate": "",
"EndDate": "",
"Duration": 36,
"CaloriesBurned": 33,
"HeartPoints": 86,
"ActivityScore": 345,
"StepCount": 567,
"StepFitPoints": 345,
"Distance": 3456,
"AveragePace": 4567

},
{"UserID": "6", 
"ActivityName": "ActivityName1",
"ActivityID": "6_1",                
"StartDate": "",
"EndDate": "",
"Duration": 36,
"CaloriesBurned": 33,
"HeartPoints": 86,
"ActivityScore": 345,
"StepCount": 567,
"StepFitPoints": 345,
"Distance": 3456,
"AveragePace": 4567

},
{"UserID": "7", 
"ActivityName": "ActivityName1",
"ActivityID": "7_1",                
"StartDate": "",
"EndDate": "",
"Duration": 36,
"CaloriesBurned": 33,
"HeartPoints": 86,
"ActivityScore": 345,
"StepCount": 567,
"StepFitPoints": 345,
"Distance": 3456,
"AveragePace": 4567

},
{"UserID": "7", 
"ActivityName": "ActivityName1",
"ActivityID": "7_2",                
"StartDate": "",
"EndDate": "",
"Duration": 36,
"CaloriesBurned": 33,
"HeartPoints": 86,
"ActivityScore": 345,
"StepCount": 567,
"StepFitPoints": 345,
"Distance": 3456,
"AveragePace": 4567

}
];

let calander =[{
    "Date": "02-07-2021",
    "UserID": "1",
    "ActivityIDs": [
        "1_3"
    ],
    "DailyFitScore": 89
},
{
    "Date": "02-11-2021",
    "UserID": "1",
    "ActivityIDs": [
        "1_1","1_2"
        
    ],
    "DailyFitScore": 34
},
{
    "Date": "02-01-2021",
    "UserID": "1",
    "ActivityIDs": [
        "1_4"
        
    ],
    "DailyFitScore": 35
},
{
    "Date": "01-01-2021",
    "UserID": "1",
    "ActivityIDs": [
        "1_5"
        
    ],
    "DailyFitScore": 37
},
{
    "Date": "07-02-2021",
    "UserID": "3",
    "ActivityIDs": [
        "3_1"
    ],
    "DailyFitScore": 123
},
{
    "Date": "07-02-2021",
    "UserID": "4",
    "ActivityIDs": [
        "4_1"
    ],
    "DailyFitScore": 345
},
{
    "Date": "07-02-2021",
    "UserID": "5",
    "ActivityIDs": [
        "5_1"
    ],
    "DailyFitScore": 344
},
{
    "Date": "07-02-2021",
    "UserID": "6",
    "ActivityIDs": [
        "6_1"
    ],
    "DailyFitScore": 342
},
{
    "Date": "07-02-2021",
    "UserID": "7",
    "ActivityIDs": [
        "7_1"
    ],
    "DailyFitScore": 341
},
{
    "Date": "05-02-2021",
    "UserID": "7",
    "ActivityIDs": [
        "7_2"
    ],
    "DailyFitScore": 313
}];



users.forEach(user => {
 userRef.doc(user.UserID).set(user);
});
calander.forEach(cal => {
    calendarRef.doc(cal.Date).set(cal);
});

activites.forEach(act => {
    activitiesRef.doc(act.ActivityID).set(act);
});


return response;

}