const functions = require('firebase-functions');
const moment = require("moment")
const axios = require("axios")
const _ = require("lodash")
//const Query = require("Query")

//const admin = require('firebase-admin');
const constants = require('./constants');
const { DATE_KEY, DATE_FORMAT, DAILY_FIT_SCORE_KEY , WEEKLY_FIT_SCORE_KEY, USER_ID_KEY} = require('./constants');
// admin.initializeApp();
//const db = admin.firestore();

// const getGoogleFitData = require('./getGoogleFitData');
// const getFitBitData = require('./getFitBitData');
// const getGarminData = require('./getGarminData');

exports.getLeaderBoard = async function (req, res, db) {
   console.log("inside get LeaderBoard data", req.body.userIds)

   let userIds = req.body.userIds
   const sortBy = req.body.sortBy


   if(userIds && typeof(userIds) == "string"){
      console.log("user Ids are comming as string")
      try{
         userIds = JSON.parse(userIds);}
      catch(e){
         userIds = [""]
      }
   }
   console.log("user Ids To be used: ",  userIds)
   let response = {}
   response.status = "Success";    
   response.message = "Expected Data Recived";
   let responseBody = {};
   
   try{
     responseBody = {
     dailyLeaderBoard : await getLeaderBoardData(userIds, db, false),
     weeklyLeaderBoard : await getLeaderBoardData(userIds, db, true)
     }
   } catch(e) {
      response.status = "Failed";
      response.message = e.message;
      return response;
   }
   response.responseBody = responseBody;
   return response;
}


//Query in Users collections as weekly fitscore is available at User's level
var getLeaderBoardData = async function (userIds, db, isWeekly) {
   let leaderBoard = [];
   //get closedonce from db
   const usersDb = await getUserDataFromDb(userIds, db, isWeekly);

   usersDb.forEach(doc => {

      let fits = isWeekly ? doc.data().WeeklyFitScore : doc.data().DailyFitScore
      const user = {
         userId : doc.data().UserID,
         fitScore : fits ? fits : 0
      }  
      leaderBoard.push(user)
   });
   return leaderBoard;
}

var getUserDataFromDb = async function(userIds, db, isWeekly) {
   let usersDb = [];
   if(isWeekly) {
      try{
         if(userIds != null && userIds.length > 0) {
            console.log("inside weekly")
         let i,j,userIdsChunk,chunk = 10;
         for (i=0,j=userIds.length; i<j; i+=chunk) {

         userIdsChunk = userIds.slice(i,i+chunk);
         usersDb1 = await db.collection("users").where(USER_ID_KEY, "in", userIdsChunk)
                                                .orderBy(WEEKLY_FIT_SCORE_KEY, "desc")
                                                .get();  

         usersDb1.forEach(doc => {usersDb.push(doc)});
   }
         } else {
            //If user ids not present in input get all users
            usersDb = await db.collection("users").orderBy(WEEKLY_FIT_SCORE_KEY, "desc")
                                                  .get();          
         }
      } catch(e){
            throw e;
      }
   } else {
            let today = moment().utcOffset("+05:30").format(DATE_FORMAT);
            console.log("Todays date ", today)
            try{
               if(userIds != null && userIds.length > 0) {
                  console.log("inside daily")
               
               let i,j,userIdsChunk,chunk = 10;
               for (i=0,j=userIds.length; i<j; i+=chunk) {
         
                  userIdsChunk = userIds.slice(i,i+chunk);
                  usersDb1 = await db.collection("calendar").where(DATE_KEY, "==" ,today)
                                                        .where(USER_ID_KEY, "in", userIdsChunk)
                                                        .orderBy(DAILY_FIT_SCORE_KEY, "desc")
                                                        .get();    
                  usersDb1.forEach(doc => {usersDb.push(doc)});   
                  
                  }} else {
               //If user ids not present in input get all users
               usersDb = await db.collection("calendar").where(DATE_KEY, "==" ,today)
                                                        .orderBy(DAILY_FIT_SCORE_KEY, "desc")
                                                        .get();          
               }
            } catch(e){
            throw e;
             }
      }
   return usersDb;
   }
