// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();
db.settings({ ignoreUndefinedProperties: true })
const runtimeOpts = {
    timeoutSeconds: 540,
}

const getUserActivity = require('./getUserActivity');
const updateActivityData = require('./updateActivity/updateActivityData');

//const getPostsByUser = require("./getPostsByUser")
const getClosedOnes = require('./getClosedOnes')
const getLeaderBoard = require('./getLeaderBoard')
const getUserRedirectURL = require('./updateActivity/garminConnect/garminDataRetrieval');
//const updateDataForTest =require('./Utilities/updateDataForTest')

// const subscribeFitbit = require("./subscribeFitbit")

// exports.getActivityData = functions.https.onRequest(async (req, res) => {
//     var callback = await getActivityData.getActivityData(req, res, db);
//     res.json({result: callback});
// });
exports.getUserActivity = functions.https.onRequest(async (req, res) => {
    var callback = await getUserActivity.getUserActivity(req, res, db);
    res.json({result: callback});
});

exports.performGarminAuth = functions.https.onRequest(async (req, res) => {
    var data = req.body;
    console.log("API request", data)
    var callback = await getUserRedirectURL.getUserToken(data);
    res.json(callback);
});


exports.updateActivityData = functions.https.onRequest(async (req, res) => {
    var callback = await updateActivityData.updateActivityData(req, res, db);
    res.json({result: callback});
});
exports.getClosedOnes = functions.https.onRequest(async (req, res) => {
    var callback = await getClosedOnes.getClosedOnes(req, res, db);
    res.json({result: callback});
});
exports.getLeaderBoard = functions.https.onRequest(async (req, res) => {
    var callback = await getLeaderBoard.getLeaderBoard(req, res, db);
    res.json({result: callback});
});
// exports.updateDataForTest = functions.https.onRequest(async (req, res) => {
//     var callback = await updateDataForTest.updateDataForTest(req, res, db);
//     res.json({result: callback});
// });
// exports.getGoogleFitData = functions.https.onRequest(async (req, res) => {
//     var callback = await getGoogleFitData.getGoogleFitData(req, res, db);
//     res.json({result: callback});
// });
// exports.saveAccessToken = functions.runWith(runtimeOpts).https.onRequest(async (req, res) => {
//     var callback = await saveAccessToken.saveAccessToken(req, res, db);
//     res.send(callback)
// });
exports.updateActivityData = functions.runWith(runtimeOpts).https.onRequest(async (req, res) => {
    var callback = await updateActivityData.updateActivityData(req, res, db);
    res.send(callback)
});
// exports.getFitBitData = functions.https.onRequest(async (req, res) => {
//     var callback = await getFitBitData.getFitBitData(req, res, db);
//     res.json({result: callback});
// });
//

// exports.scheduledFunctionCrontab = functions.pubsub.schedule('0 0 * * SUN')
//   .timeZone('Asia/Kolkata') 
//   .onRun((context) => {
//     const collection = db.collection("users").get()
//     collection.forEach(doc=> {doc.ref.update({WeeklyFitScore : 0})})
//     console.log("Running scheduledFunctionCrontab to update WeeklyFitScore = 0")
// });
// exports.subscribeFitbit = subscribeFitbit.subscribeFitbit


// exports.getPostsByUser = getPostsByUser.getPostsByUser



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	