
const moment = require("moment");
const axios = require("axios");
const async = require("async");
const _ = require("lodash")
exports.saveAccessToken = async function (req, res, db) {
    const userID = req.body.userID
    const token = req.headers.authorization
    const PrimarySourceID = req.body.sourceID || "1"
    const PrimarySourceName = req.body.primarySourceName || "GoogleFit"
    const startDate = req.body.startDate
    console.log("Start date ", startDate )
    const endDate = req.body.endDate
    console.log("end date ", endDate )
    var apiStartDate;
    var apiActivities = []
    var resultActivities = []
    var calendar = []
    var callback = {}
    try {
        
        const userRef = db.collection('users');
        const calendarRef = db.collection("calendar")
        const activitiesRef = db.collection("activities")
        const calendarDetails = await calendarRef.where("UserID", "==", userID).get();
        const userDetails = await userRef.where('UserID', '==', userID).get();
        
        var LastActivityUpdated;
        if (userDetails.empty) {
            apiStartDate = startDate
            totalNumberOfActivities = 0
            
        } else {
            userDetails.forEach(doc => {
                LastActivityUpdated = doc.data().LastActivityUpdated.toDate()
                totalNumberOfActivities = doc.data().TotalNumberOfActivities
            });
            console.log("Date 1 ", moment(endDate, "MM-DD-YYYY").format("MM-DD-YYYY"))
            console.log("Date 2 ", moment(LastActivityUpdated).format("MM-DD-YYYY"))
            if (moment(endDate, "MM-DD-YYYY").isAfter(moment(LastActivityUpdated))) {
                activitiesDbEndDate = LastActivityUpdated
            } else {
                activitiesDbEndDate = endDate
            }
            
            if (moment(endDate, "MM-DD-YYYY").isAfter(moment(LastActivityUpdated))) {
                apiStartDate = startDate
            } else {
                if (moment(endDate, "MM-DD-YYYY").isAfter(moment(LastActivityUpdated))) {
                    apiStartDate = LastActivityUpdated
                } else {
                    apiStartDate = endDate
                }
            }
        }

        if (PrimarySourceID == 1) {
            console.log("API start date ", moment(apiStartDate, "MM-DD-YYYY").format("MM-DD-YYYY"))
            // google fit data
            var data = {
                "aggregateBy": 
                [
                    {
                        "dataTypeName": "com.google.step_count.delta",
                    },
              
                    {
              
                        "dataTypeName": "com.google.calories.expended",
                
                    },
                    {
                        "dataTypeName": "com.google.heart_minutes",
                    },
                    {
                        "dataTypeName": "com.google.distance.delta"
                    },
              
                ],
                "bucketByActivitySegment": {
                    "minDurationMillis": 180000
                },
                "startTimeMillis": moment(apiStartDate, "MM-DD-YYYY").valueOf(),
                "endTimeMillis": moment(endDate, "MM-DD-YYYY").valueOf()
            }
            await axios.post('https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate',data, {
                headers: {  
                    "Authorization" : token
                }    
            
            })
            .then(function (response) {
                // handle success
                console.log("reached here 2")
                var result = response.data
                if (result.bucket && result.bucket.length > 0) {
                    for (var i =0;i<result.bucket.length;i++) {
                        var obj = {}
                        totalNumberOfActivities++ 
                        obj.ActivityID = userID + "_" + totalNumberOfActivities.toString()
                        obj.ActivityName = constants[result.bucket[i].activity]
                        obj.StartDate = moment(Number(result.bucket[i].startTimeMillis)).format("MM-DD-YYYY hh:mm:ss")
                        obj.EndDate = moment(Number(result.bucket[i].endTimeMillis)).format("MM-DD-YYYY hh:mm:ss")
                        if (result.bucket[i].dataset && result.bucket[i].dataset.length > 0) {
                            for (var j=0;j<result.bucket[i].dataset.length;j++) {
                                if (result.bucket[i].dataset[j].point && result.bucket[i].dataset[j].point.length > 0) {
                                    if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.calories.expended:com.google.android.gms:aggregated") {
                                        // count calories 
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.CaloriesBurned = result.bucket[i].dataset[j].point[0].value[0].fpVal    
                                        }
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.heart_minutes.summary:com.google.android.gms:aggregated") {
                                        // count heart points 
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.HeartPoints = result.bucket[i].dataset[j].point[0].value[0].fpVal
                                            console.log("Duration ", result.bucket[i].dataset[j].point[0].value[1])
                                            obj.Duration = result.bucket[i].dataset[j].point[0].value[1].intVal/60
                                            if (obj.Duration) {
                                                obj.ActivityScore = calculateActivityScore(obj.HeartPoints, obj.Duration)
                                            }
                                            
                                        }
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.step_count.delta:com.google.android.gms:aggregated") {
                                        // count steps
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.StepCount = result.bucket[i].dataset[j].point[0].value[0].intVal
                                            obj.StepFitPoints = calculateFitPoints(obj.StepCount)
                                        }
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.distance.delta:com.google.android.gms:aggregated") {
                                        // count distance and average pace
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.Distance = result.bucket[i].dataset[j].point[0].value[0].fpVal/1000
                                            console.log("Distance ", obj.Distance)
                                            
                                            if (obj.Duration) {
                                                obj.AveragePace = obj.Distance/obj.Duration
                                            }
                                        }  
                                    } 
                                }   
                            }
                        }
                        apiActivities.push(obj)
                    }
                } else {
                    // no data
                }
                
            })
            .catch(function (error) {
                // handle error
                console.log("Error Excpetion api 1 ", error)
                callback = {
                    success: false,
                    status: 400,
                    error: error,
                    request: data,
                }
            })
            var stepData = {
                "aggregateBy": [{
                    "dataTypeName": "com.google.step_count.delta",
                    "dataSourceId": "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps"
                }],
                "bucketByTime": { "durationMillis": 86400000 },
                "startTimeMillis": moment(apiStartDate,"MM-DD-YYYY").valueOf(),
                "endTimeMillis": moment(endDate, "MM-DD-YYYY").valueOf()
            }
            console.log("Body for second api ", stepData)
            await axios.post('https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate',stepData, {
                headers: {
                    "Authorization" : token
                }    
            
            })
            .then(function (response){
                var result = response.data
                console.log("step data", result.bucket)
                if (result && result.bucket && result.bucket.length > 0) {
                    for (var i=0;i<result.bucket.length;i++) {
                        if (result.bucket[i].dataset && result.bucket[i].dataset.length > 0 && result.bucket[i].dataset[0].point && result.bucket[i].dataset[0].point.length > 0) {
                            var obj = {}
                            obj.ActivityID = userID + "_" + (totalNumberOfActivities + 1)
                            totalNumberOfActivities++ 
                            obj.ActivityName = "Daily Steps"
                            obj.StartDate = moment(Number(result.bucket[i].startTimeMillis)).format("MM-DD-YYYY hh:mm:ss")
                            obj.EndDate = moment(Number(result.bucket[i].endTimeMillis)).format("MM-DD-YYYY hh:mm:ss")
                            obj.ActivityID = 100
                            if (result.bucket[i].dataset[0].point[0].value && result.bucket[i].dataset[0].point[0].value.length > 0) {
                                obj.StepCount = result.bucket[i].dataset[0].point[0].value[0].intVal
                                obj.StepFitPoints = calculateFitPoints(obj.StepCount)
                            }
                            apiActivities.push(obj)
                        }
                    }
                }
            })
            .catch(function (error){
                console.log("error in second api ", error)
            })
            if (userDetails.empty) {
                await userRef.doc(userID.toString()).set({
                    UserID: userID, PrimarySourceID: '1', PrimarySourceName: 'GoogleFit',
                    LastActivityUpdated: new Date()
                });
            } else {
                await userRef.doc(userID.toString()).update({
                    LastActivityUpdated: new Date()
                });
            }
            console.log("end this here ", apiActivities.length)

            for (i=0;i<apiActivities.length;i++) {
                
                await activitiesRef.doc().set({
                    UserID: userID, 
                    ActivityName: apiActivities[i].ActivityName, 
                    ActivityID: apiActivities[i].ActivityID, 
                    StartDate: apiActivities[i].StartDate ? new Date(apiActivities[i].StartDate) : undefined,
                    EndDate: apiActivities[i].EndDate ? new Date(apiActivities[i].EndDate) : undefined,
                    Duration: apiActivities[i].Duration,
                    CaloriesBurned: apiActivities[i].CaloriesBurned,
                    HeartPoints: apiActivities[i].HeartPoints,
                    ActivityScore: apiActivities[i].ActivityScore,
                    StepCount: apiActivities[i].StepCount,
                    StepFitPoints: apiActivities[i].StepFitPoints,
                    Distance: apiActivities[i].Distance,
                    AveragePace: apiActivities[i].AveragePace
                });
            }
        } else if (PrimarySourceID == 2) {
            // update fitbit data
            let userExists = false;
            var sourceUserID = req.body.sourceUserID
            if(!userDetails.empty) {
                userExists = true;
            }
            let requestUrl = 'https://api.fitbit.com/1/user/' + sourceUserID + '/activities/list.json?';
    //         let requestUrl = 'https://api.fitbit.com/1/user/' + "-";
            if(!userExists) {
                requestUrl = requestUrl + 'beforeDate=' + moment().format('YYYY-MM-DD');
            } else {
                requestUrl = requestUrl + 'afterDate=' + moment(userDetails.LastActivityUpdated).format('YYYY-MM-DD');
            }
            requestUrl = requestUrl + '&sort=desc&limit=20&offset=0';
            console.log("Reached here 1")
            await axios.get(requestUrl,{
                headers: {
                    Authorization:token
                }
            })
            .then(function (response){
                var result = response.data
                if (result.activities && result.activities.length > 0) {
                    console.log("length ", result.activities.length)
                    for (var i=0;i<result.activities.length;i++) {
                        var obj = {}
                        totalNumberOfActivities++ 
                        obj.ActivityID = userID + "_" + totalNumberOfActivities.toString()
                        obj.ActivityName = result.activities[i].activityName
                        obj.Duration = result.activities[i].activeDuration/60000
                        obj.CaloriesBurned = result.activities[i].calories
                        obj.StepCount = result.activities[i].steps
                        obj.Distance = result.activities[i].distance
                        obj.AveragePace = result.activities[i].speed
                        obj.HeartPoints = result.activities[i].averageHeartRate
                        if (obj.Duration) {
                            obj.ActivityScore = calculateActivityScore(obj.HeartPoints, obj.Duration)
                        }
                        obj.StepFitPoints = calculateFitPoints(obj.StepCount)
                        obj.StartDate = moment(result.activities[i].startTime, "YYYY-MM-DD").format("MM-DD-YYYY hh:mm:ss")
                        apiActivities.push(obj)
                    }
                }
                
            })
            .catch(function (error){
                console.log("error in second api ", error)
                callback = {
                    success: false,
                    status: 400,
                    error: error,
                    request: data,
                }
            })
            for (i=0;i<apiActivities.length;i++) {
                
                await activitiesRef.doc().set({
                    UserID: userID, 
                    ActivityName: apiActivities[i].ActivityName, 
                    ActivityID: apiActivities[i].ActivityID, 
                    StartDate: apiActivities[i].StartDate ? new Date(apiActivities[i].StartDate) : undefined,
                    EndDate: apiActivities[i].EndDate ? new Date(apiActivities[i].EndDate) : undefined,
                    Duration: apiActivities[i].Duration,
                    CaloriesBurned: apiActivities[i].CaloriesBurned,
                    HeartPoints: apiActivities[i].HeartPoints,
                    ActivityScore: apiActivities[i].ActivityScore,
                    StepCount: apiActivities[i].StepCount,
                    StepFitPoints: apiActivities[i].StepFitPoints,
                    Distance: apiActivities[i].Distance,
                    AveragePace: apiActivities[i].AveragePace
                });
            }
        }
        
        if (userDetails.empty) {
            await userRef.doc(userID.toString()).set({
                UserID: userID, PrimarySourceID: PrimarySourceID, PrimarySourceName: PrimarySourceName,
                LastActivityUpdated: new Date(),
                AccessToken: token,
                TotalNumberOfActivities: totalNumberOfActivities
            });
        } else {
            await userRef.doc(userID.toString()).update({
                LastActivityUpdated : new Date(),
                TotalNumberOfActivities: totalNumberOfActivities
            });
        }
        console.log("api activities ", apiActivities)
        if (calendarDetails.empty) {
            console.log("inside calenadr")
            var activityByDate = _.groupBy(apiActivities, function (o) {
                return moment(o.StartDate, "MM-DD-YYYY").format("MM-DD-YYYY")
            })
            console.log("activities by date ", activityByDate)
            for (var key in activityByDate) {
                console.log("date inside ", key)
                var obj
                if (PrimarySourceID == 1) {
                    obj = {
                        StartDate : moment(key),
                        UserID: userID
                    }    
                } else {
                    obj = {
                        StartDate : moment(key, "MM-DD-YYYY"),
                        UserID: userID
                    }
                }
                
                var dailyFitScore = 0
                var activityIDs = []
                for (var i=0;i<activityByDate[key].length;i++) {
                    if (!isNaN(activityByDate[key][i].StepFitPoints)) {
                        dailyFitScore = activityByDate[key][i].StepFitPoints + dailyFitScore
                    }
                    activityIDs.push(activityByDate[key][i].ActivityID)
                }
                obj.DailyFitScore = dailyFitScore
                obj.ActivityIDs = activityIDs
                calendar.push(obj)
            }
            for (var i =0;i<calendar.length;i++) {
                await calendarRef.doc().set({
                    StartDate: calendar[i].StartDate,
                    UserID: calendar[i].UserID,
                    ActivityIDs: calendar[i].ActivityIDs,
                    DailyFitScore: calendar[i].DailyFitScore
                });
            }
            console.log("inside calenadr written")
            
        } else {
            var activityByDate = _.groupBy(apiActivities, function (o) {
                return o.StartDate.substr(0,10)
            })
            for (var key in activityByDate) {
                var obj = {
                    StartDate : moment(key),
                    UserID: userID
                }
                var dailyFitScore = 0
                var activityIDs = []
                for (var i=0;i<activityByDate[key].length;i++) {
                    if (!isNaN(activityByDate[key][i].StepFitPoints)) {
                        dailyFitScore = activityByDate[key][i].StepFitPoints + dailyFitScore
                    }
                    activityIDs.push(activityByDate[key][i].ActivityID)
                }
                obj.DailyFitScore = dailyFitScore
                obj.ActivityIDs = activityIDs
                calendar.push(obj)
            }
            for (var i =0;i<calendar.length;i++) {
                await calendarRef.doc().set({
                    StartDate: calendar[i].StartDate,
                    UserID: calendar[i].UserID,
                    ActivityIDs: calendar[i].ActivityIDs,
                    DailyFitScore: calendar[i].DailyFitScore
                });
            }
        }
        callback = {
            data: "Token updated successfully",
            status: 200,
            result: true
        }
    } catch (e) {
        console.log("Exception 2 ", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }
    return callback
};

var calculateFitPoints = function (steps) {
    return steps * 0.002
    // var score = 0;
    // if (steps < 2500) {
    //     score = 0 
    // } else if (steps >=2500 && steps < 5000) {
    //     score = 0.6 * ((steps - 2500)/ 500)
    // } else if (steps >= 5000 && steps < 7500) {
    //   score = 0.6 * ((steps - 2500)/ 500)
    // } else if (steps >= 7500 && steps < 10000) {
    //     score = 0.6 * 30
    // } else if (steps >= 10000 && steps < 12500) {
    //     score = 0.6 * (30 + ((steps - 10000)/ 500))
    // } else {
    //   score = 0.6 * (35 + ((steps - 12500)/ 500))
    // }
    // return score
}

var calculateActivityScore = function (heartPoints, duration) {
    var activityScore = 0
    if (heartPoints > duration) {
        activityScore = heartPoints
    } else {

    }
    return activityScore
}

var constants = {
  "9" : "Aerobics",
  "119" : "Archery",
  "10" : "Badminton",
  "11" : "Baseball",
  "12" : "Basketball",
  "13" : "Biathlon",
  "1" : "Biking",
  "14" : "Handbiking",
  "15" : "Mountain biking",
  "16" : "Road biking",
  "17" : "Spinning",
  "18" : "Stationary biking",
  "19" : "Utility biking",
  "20" : "Boxing",
  "21" : "Calisthenics",
  "22" : "Circuit training",
  "23" : "Cricket",
  "113" : "Crossfit",
  "106" : "Curling",
  "24" : "Dancing",
  "102" : "Diving",
  "117" : "Elevator",
  "25" : "Elliptical",
  "103" : "Ergometer",
  "118" : "Escalator",
  "26" : "Fencing",
  "27" : "Football (American)",
  "28" : "Football (Australian)",
  "29" : "Football (Soccer)",
  "30" : "Frisbee",
  "31" : "Gardening",
  "32" : "Golf",
  "122" : "Guided Breathing",
  "33" : "Gymnastics",
  "34" : "Handball",
  "114" : "HIIT",
  "35" : "Hiking",
  "36" : "Hockey",
  "37" : "Horseback riding",
  "38" : "Housework",
  "104" : "Ice skating",
  "0" : "In vehicle",
  "115" : "Interval Training",
  "39" : "Jumping rope",
  "40" : "Kayaking",
  "41" : "Kettlebell training",
  "42" : "Kickboxing",
  "43" : "Kitesurfing",
  "44" : "Martial arts",
  "45" : "Meditation",
  "46" : "Mixed martial arts",
  "108" : "Other (unclassified fitness activity)",
  "47" : "P90X exercises",
  "48" : "Paragliding",
  "49" : "Pilates",
  "50" : "Polo",
  "51" : "Racquetball",
  "52" : "Rock climbing",
  "53" : "Rowing",
  "54" : "Rowing machine",
  "55" : "Rugby",
  "8" : "Running",
  "56" : "Jogging",
  "57" : "Running on sand",
  "58" : "Running (treadmill)",
  "59" : "Sailing",
  "60" : "Scuba diving",
  "61" : "Skateboarding",
  "62" : "Skating",
  "63" : "Cross skating",
  "105" : "Indoor skating",
  "64" : "Inline skating (rollerblading)",
  "65" : "Skiing",
  "66" : "Back-country skiing",
  "67" : "Cross-country skiing",
  "68" : "Downhill skiing",
  "69" : "Kite skiing",
  "70" : "Roller skiing",
  "71" : "Sledding",
  "73" : "Snowboarding",
  "74" : "Snowmobile",
  "75" : "Snowshoeing",
  "120" : "Softball",
  "76" : "Squash",
  "77" : "Stair climbing",
  "78" : "Stair-climbing machine",
  "79" : "Stand-up paddleboarding",
  "3" : "Still (not moving)",
  "80" : "Strength training",
  "81" : "Surfing",
  "82" : "Swimming",
  "84" : "Swimming (open water)",
  "83" : "Swimming (swimming pool)",
  "85" : "Table tennis (ping pong)",
  "86" : "Team sports",
  "87" : "Tennis",
  "5" : "Tilting (sudden device gravity change)",
  "88" : "Treadmill (walking or running)",
  "4" : "Unknown (unable to detect activity)",
  "89" : "Volleyball",
  "90" : "Volleyball (beach)",
  "91" : "Volleyball (indoor)",
  "92" : "Wakeboarding",
  "7" : "Walking",
  "93" : "Walking (fitness)",
  "94" : "Nording walking",
  "95" : "Walking (treadmill)",
  "116" : "Walking (stroller)",
  "96" : "Waterpolo",
  "97" : "Weightlifting",
  "98" : "Wheelchair",
  "99" : "Windsurfing",
  "100" : "Yoga",
  "101" : "Zumba",
}