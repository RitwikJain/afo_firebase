const moment = require("moment");
const axios = require("axios");
const _ = require("lodash")
const googleFitMapping = require("../activityMappers/googleFitMapping");
const activityMasterData = require("../activityMappers/activityMasterData");
const { DATE_KEY, DATE_FORMAT,ACTIVITY_ID_KEY,ACTIVITY_ID_DATE_TIME_FORMAT } = require("../constants");

const getGoogleFitData = async function( updateAPIRequest ) {
    let token = updateAPIRequest.token
    let apiStartDate = updateAPIRequest.apiStartDate
    let apiEndDate = updateAPIRequest.apiEndDate
    let userID = updateAPIRequest.userID
    let lastActivityUpdated = updateAPIRequest.lastActivityUpdated
    let totalNumberOfActivities = updateAPIRequest.totalNumberOfActivities
    let activitiesRef = updateAPIRequest.activitiesRef

    console.log("Google handler is called with request :", updateAPIRequest)

    let apiActivities = []
        console.log("inside google fit data")
    
        var stepData = {
            "aggregateBy": [{
                "dataTypeName": "com.google.step_count.delta",
                "dataSourceId": "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps"
            }],
            "bucketByTime": { "durationMillis": 86400000 },
            "startTimeMillis": apiStartDate.valueOf(),
            "endTimeMillis": apiEndDate.valueOf()
    
        }
        console.log("Body for second api ", stepData)
        await axios.post('https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate',stepData, {
            headers: {
                "Authorization" : token
            }    
        })
        .then(async function (response){
            var result = response.data
            console.log("step data", result.bucket)
            if (result && result.bucket && result.bucket.length > 0) {
                console.log("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq")
                console.log("bucketsize = ", result.bucket.length)
                console.log("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq")
                for (let i=0;i<result.bucket.length;i++) {
                    if (result.bucket[i].dataset && result.bucket[i].dataset.length > 0 && result.bucket[i].dataset[0].point && result.bucket[i].dataset[0].point.length > 0) {
                        let obj = {}
                        let activityID = userID + "_" +  moment(Number(result.bucket[i].startTimeMillis)).utcOffset("+05:30").format(ACTIVITY_ID_DATE_TIME_FORMAT)
                        obj.ActivityID = activityID
                        obj.ActivityName = "Daily Steps"
                            
                        obj.StartDate = moment(Number(result.bucket[i].startTimeMillis)).startOf('day').valueOf()
                        obj.EndDate = moment(Number(result.bucket[i].endTimeMillis)).startOf('day').valueOf()
                        //obj.ActivityID = 100
                        if (result.bucket[i].dataset[0].point[0].value && result.bucket[i].dataset[0].point[0].value.length > 0) {
                            obj.StepCount = result.bucket[i].dataset[0].point[0].value[0].intVal
                            obj.StepFitPoints = calculateGoogleStepFitPoints(obj.StepCount)
                            obj.Source = result.bucket[i].dataset[0].point[0].originDataSourceId ? result.bucket[i].dataset[0].point[0].originDataSourceId : undefined
                        }
                        apiActivities.push(obj)
                    }
                }
            }
        })
        .catch(function (error){
            console.log("error in steps API call ", error)
            throw error
        })  
        //Get Activities data   
        var data = {
            "aggregateBy": 
            [
                {
                    "dataTypeName": "com.google.step_count.delta",
                },
                {
                    "dataTypeName": "com.google.calories.expended",
                },
                {
                    "dataTypeName": "com.google.heart_minutes",
                },
                {
                    "dataTypeName": "com.google.distance.delta"
                },
            ],
            "bucketByActivitySegment": {
                "minDurationMillis": 180000
            },
            "startTimeMillis": apiStartDate.valueOf(),
            "endTimeMillis": apiEndDate.valueOf()
        }
        await axios.post('https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate',data, {
            headers: {  
                "Authorization" : token
            }    
        
        })
        .then(async function (response) {
            // handle success
            console.log("Got response from google fit",response)
            let result = response.data

            let prevActivity = {}
            if (result.bucket && result.bucket.length) {
                for (let i =0;i<result.bucket.length;i++) {
                    let obj = {}
                    //ActivityID will be combination of UserID and timestamp
                    let activityID = userID + "_" +  moment(Number(result.bucket[i].startTimeMillis)).utcOffset("+05:30").format(ACTIVITY_ID_DATE_TIME_FORMAT)
                    obj.ActivityID = activityID
                    let activityName =  googleFitMapping[result.bucket[i].activity]

                  console.log("---------------------------")
                  console.log("UserActivity Name googlefit ", result.bucket[i].activity)
                  console.log("UserActivity Name master", activityName)
                  console.log("---------------------------")
                  let activityConfig = activityMasterData[activityName]
                    if(activityName && activityConfig) {
                        console.log("UserActivity Config",activityConfig )
                        let metValue = activityConfig.METsValue;
                        obj.ActivityName = activityName

                        obj.StartDate = Number(result.bucket[i].startTimeMillis)
                        obj.EndDate = Number(result.bucket[i].endTimeMillis)
                        let activityDuration = Math.round(moment.duration(obj.EndDate - obj.StartDate).asMinutes())
                        obj.Duration = activityDuration

                        if (result.bucket[i].dataset && result.bucket[i].dataset.length > 0) {
                            for (let j=0;j<result.bucket[i].dataset.length;j++) {
                                if (result.bucket[i].dataset[j].point && result.bucket[i].dataset[j].point.length > 0) {
                                    if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.calories.expended:com.google.android.gms:aggregated") {
                                    // count calories 
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.CaloriesBurned = Number(result.bucket[i].dataset[j].point[0].value[0].fpVal) 
                                        }
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.heart_minutes.summary:com.google.android.gms:aggregated") {
                                        // count heart points 
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.HeartPoints = Number(result.bucket[i].dataset[j].point[0].value[0].fpVal)
                                       // console.log("Duration ", result.bucket[i].dataset[j].point[0].value[1])
                                          //  obj.Duration = result.bucket[i].dataset[j].point[0].value[1].intValue
                                           obj.ActivityScore = calculateGoogleActivityScore(obj.HeartPoints, obj.Duration, metValue)
                                            }
                                        
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.step_count.delta:com.google.android.gms:aggregated") {
                                    // count steps
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                            obj.StepCount = result.bucket[i].dataset[j].point[0].value[0].intVal
                                            obj.StepFitPoints = calculateGoogleStepFitPoints(obj.StepCount)
                                        }
                                    } else if (result.bucket[i].dataset[j].dataSourceId == "derived:com.google.distance.delta:com.google.android.gms:aggregated") {
                                       
                                        // count distance and average pace
                                        //TODO : min/km for walking and running, km/hr for cycling
                                        if (result.bucket[i].dataset[j].point[0].value && result.bucket[i].dataset[j].point[0].value.length > 0) {
                                           //Distance in meters
                                            obj.Distance = Number(result.bucket[i].dataset[j].point[0].value[0].fpVal)

                                            if(obj.Duration){
                                                if(activityName == "Walking" || activityName == "Running"){
                                                //min/km
                                                obj.AveragePace = Math.round((obj.Duration/obj.Distance) * 1000)
                                                } else {
                                                //For other distance based activities : km/hr
                                                obj.AveragePace = Math.round((obj.Distance/obj.Duration) * 60 )
                                                }
                                            }   
                                        }  
                                    }
                                    //get The source of activity 
                                    obj.Source = result.bucket[i].dataset[j].point[0].originDataSourceId     
                                }   
                            }    
                        }
                        if(activityName == "Walking" || activityName == "Running"){

                           obj.ActivityScore = obj.AveragePace < 8.50 ? ( 2 * obj.Duration) : obj.Duration

                        }
                        //If heart points are not given
                        if(!obj.ActivityScore){
                            obj.ActivityScore = calculateGoogleActivityScore(0, activityDuration, metValue)
                        }     
                        if(lastActivityUpdated && moment(obj.StartDate).utcOffset("+05:30").isSameOrBefore(lastActivityUpdated)) {
                            let exisingActivity = await activitiesRef.where("ActivityID","==", activityID).get();
                            if(exisingActivity && exisingActivity.length > 0){
                                 // if already exist do not increase activity count
                            } else{                        
                            totalNumberOfActivities++ 
                            }
                        } else { 
                        totalNumberOfActivities++ 
                        }
                        if(activityName == "Walking" || activityName == "Running") {
                           if(obj.Distance >= 0.5){
                            apiActivities.push(obj)
                           } else {
                            totalNumberOfActivities -- 
                           }
                        }else {
                        apiActivities.push(obj)
                        }
                       
                    } else {

                        //Name not found in mapping
                        console.log("Invalid activity")
                    }
                }
            } else {
                // no data
                console.log("no data")
            }

        })
        .catch(function (error) {
            // handle error
            console.log("Error Excpetion api 1 ", error)
            throw error;
        })

        let apiResponse = {
            success: true,
            status : 200,
            apiActivities : apiActivities,
            totalNumberOfActivities : totalNumberOfActivities
        }
    return  apiResponse;
    }   
    const calculateGoogleStepFitPoints = function(steps){
        return Math.round(steps * 0.002)
    }
    
    const calculateGoogleActivityScore = function (heartPoints, duration, metValue) {
        
        if (heartPoints > duration) {
            return heartPoints
        } else {
            if(metValue<6){
                return duration
            } else {
                return  2*duration
            }
        }
    }



