const functions = require('firebase-functions');


// const admin = require('firebase-admin');
// admin.initializeApp();
// const db = admin.firestore();

exports.getPostsByUser = functions.https.onRequest(async (req, res) => {
    // var userID = req.body.userID
    var userID = 1
    const posts = db.collection('activities').doc(userID);
    const doc = await posts.get();
    var callback = {}
    if (!doc.exists) {
        callback = {

        }
        res.json({
            result: {
                status: 200,
                statusMessage: "Success",
                headers: req.headers,
                data: {
                    userID: userID,
                    posts: doc
                }
            }
        })
    } else {
        console.log('Document data:', doc.data());
        res.json({
            result: {
                status: 200,
                statusMessage: "Success",
                headers: req.headers,
                data: {
                    userID: userID,
                    posts: []
                }
            }
        })
    }
});