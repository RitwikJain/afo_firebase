const activityMasterData = {
    "Aerobics" :{
        "masterActivityID" : 1,
        "METsValue": 6.25,
        "category":"Fitness",
        "type": "generic"
    },
    "Badminton" :{
        "masterActivityID" : 2,
        "METsValue": 5.75,
        "category":"Sports",
        "type": "generic"
    },
    "Basketball" :{
        "masterActivityID" : 3,
        "METsValue": 6.40,
        "category":"Sports",
        "type": "generic"
    },
    "Cycling Outdoors" :{
        "masterActivityID" : 4,
        "METsValue": 5.8,
        "category":"Cycling",
        "type": "speed"
    },
    "Cycling Indoors" :{
        "masterActivityID" : 5,
        "METsValue": 8.4,
        "category":"Cycling",
        "type": "speed"
    },
    "Boxing" :{
        "masterActivityID" : 6,
        "METsValue": 7.95,
        "category":"Sports",
        "type": "generic"

    },
    "Calisthenics" :{
        "masterActivityID" : 7,
        "METsValue": 5.75,
        "category":"Strength & Conditioning",
        "type": "generic"
    },
    "Circuit training" :{
        "masterActivityID" : 8,
        "METsValue": 8,
        "category":"HIIT",
        "type": "generic"
    },
    "Cricket" :{
        "masterActivityID" : 9,
        "METsValue": 5,
        "category":"Sports",
        "type": "generic"
    },
    "Crossfit" :{
        "masterActivityID" : 10,
        "METsValue": 6,
        "category":"Crossfit",
        "type": "generic"
    },
    "Dancing" :{
        "masterActivityID" : 11,
        "METsValue": 7.8,
        "category":"Dance Workout",
        "type": "generic"
    },
    "Football" :{
        "masterActivityID" : 12,
        "METsValue": 6.74,
        "category":"Sports",
        "type": "generic"
    },
    "Gymnastics" :{
        "masterActivityID" : 13,
        "METsValue": 6,
        "category":"Gymnastics",
        "type": "generic"
    },
    "HIIT" :{
        "masterActivityID" : 14,
        "METsValue": 8,
        "category":"HIIT",
        "type": "generic"
    },
    "Hiking" :{
        "masterActivityID" : 15,
        "METsValue": 7.625,
        "category":"Walking",
        "type": "distance"
    },
    "Hockey" :{
        "masterActivityID" : 16,
        "METsValue": 7.5,
        "category":"Sports",
        "type": "generic"
    },
    "Interval training" :{
        "masterActivityID" : 17,
        "METsValue": 8,
        "category":"HIIT",
        "type": "generic"
    },
    "Jumping rope" :{
        "masterActivityID" : 18,
        "METsValue": 8.375,
        "category":"HIIT",
        "type": "generic"
    },
    "Kettlebell training" :{
        "masterActivityID" : 19,
        "METsValue": 8,
        "category":"Strength & Conditioning",
        "type": "generic"
    },
    "Kickboxing" :{
        "masterActivityID" : 20,
        "METsValue": 9.39,
        "category":"Sports",
        "type": "generic"
    },
    "Martial arts" :{
        "masterActivityID" :21,
        "METsValue": 5.3,
        "category":"Sports",
        "type": "generic"
    },
    "Meditation" :{
        "masterActivityID" : 22,
        "METsValue": 1,
        "category":"Mindfullness",
        "type": "generic"
    },
    "Pilates" :{
        "masterActivityID" : 23,
        "METsValue": 4.34,
        "category":"Strength & Conditioning",
        "type": "generic"
    },
    "Rock climbing" :{
        "masterActivityID" : 24,
        "METsValue": 9,
        "category":"Adventure Sports",
        "type": "generic"
    },
    "Running" :{
        "masterActivityID" : 25,
        "METsValue": 9.8,
        "category":"Runnning",
        "type": "speed"
    },
    "Skating" :{
        "masterActivityID" : 26,
        "METsValue": 8.7,
        "category":"Skating",
        "type": "generic"
    },
    "Skiing" :{
        "masterActivityID" : 27,
        "METsValue": 8.83,
        "category":"Runnning",
        "type": "generic"
    },
    "Squash" :{
        "masterActivityID" : 28,
        "METsValue": 9.67,
        "category":"Skiing",
        "type": "generic"
    },
    "Stair climbing" :{
        "masterActivityID" : 29,
        "METsValue": 8.61,
        "category":"Stair climbing",
        "type": "generic"
    },
    "Strength training" :{
        "masterActivityID" : 30,
        "METsValue": 5.5,
        "category":"Sports",
        "type": "Strength & Conditioning"
    },
    "Swimming" :{
        "masterActivityID" : 31,
        "METsValue": 8.8,
        "category":"Swimming",
        "type": "generic"
    },
    "Table Tennis" :{
        "masterActivityID" : 32,
        "METsValue": 4,
        "category":"Table Tennis",
        "type": "generic"
    },
    "Tennis" :{
        "masterActivityID" : 33,
        "METsValue": 7,
        "category": "Tennis",
        "type": "generic"
    },
    "Volleyball" :{
        "masterActivityID" : 34,
        "METsValue": 5.5,
        "category": "Volleyball",
        "type": "generic"
    },
    "Walking" :{
        "masterActivityID" : 35,
        "METsValue": 5,
        "category": "Walking",
        "type": "generic"
    },
    "Walking Fast" :{
        "masterActivityID" : 36,
        "METsValue": 6.5,
        "category": "Walking Fast",
        "type": "generic"
    },
    "Weightlifting" :{
        "masterActivityID" : 37,
        "METsValue": 4.5,
        "category": "Weightlifting",
        "type": "generic"
    },
    "Yoga" :{
        "masterActivityID" : 38,
        "METsValue": 4.4333,
        "category": "Yoga",
        "type": "generic"
    },
    "Zumba" :{
        "masterActivityID" : 39,
        "METsValue": 8.5,
        "category": "Dance Workout",
        "type": "generic"
    },
    "Archery" :{
        "masterActivityID" : 40,
        "METsValue": 3.5,
        "category": "Sports",
        "type": "generic"
    },
    "Baseball" :{
        "masterActivityID" : 41,
        "METsValue": 4.375,
        "category": "Sports",
        "type": "generic"
    },
    "Elliptical" :{
        "masterActivityID" : 42,
        "METsValue": 9.425,
        "category": "Cardio",
        "type": "generic"
    },
    "Ergometer" :{
        "masterActivityID" : 43,
        "METsValue": 9.166,
        "category": "Cardio",
        "type": "generic"
    },
    "Frisbee" :{
        "masterActivityID" : 44,
        "METsValue": 5.5,
        "category": "Sports",
        "type": "generic"
    },
    "Golf" :{
        "masterActivityID" : 45,
        "METsValue": 3.485,
        "category": "Sports",
        "type": "generic"
    },
    "Handball" :{
        "masterActivityID" : 46,
        "METsValue": 10,
        "category": "Sports",
        "type": "generic"
    },
    "Horseback riding" :{
        "masterActivityID" : 47,
        "METsValue": 5.5,
        "category": "Sports",
        "type": "generic"
    },
    "Kayaking" :{
        "masterActivityID" : 48,
        "METsValue": 5,
        "category": "Sports",
        "type": "generic"
    },
    "Surfing" :{
        "masterActivityID" : 49,
        "METsValue": 3,
        "category": "Surfing",
        "type": "generic"
    },
    "Polo" :{
        "masterActivityID" : 50,
        "METsValue": 3,
        "category": "Sports",
        "type": "generic"
    },
    "Racquetball" :{
        "masterActivityID" : 51,
        "METsValue": 8.5,
        "category": "Sports",
        "type": "generic"
    },
    "Rowing on River" :{
        "masterActivityID" : 52,
        "METsValue": 9.5,
        "category": "Cardio",
        "type": "generic"
    },
    "Rowing Indoors" :{
        "masterActivityID" : 53,
        "METsValue": 9.166,
        "category": "Cardio",
        "type": "generic"
    },
    "Rugby" :{
        "masterActivityID" : 54,
        "METsValue": 10,
        "category": "Sports",
        "type": "generic"
    },
    "Waterpolo" :{
        "masterActivityID" : 55,
        "METsValue": 10,
        "category": "Sports",
        "type": "generic"
    },
    "Softball" :{
        "masterActivityID" : 56,
        "METsValue": 6,
        "category": "Sports",
        "type": "generic"
    },
    "Wakeboarding" :{
        "masterActivityID" : 57,
        "METsValue": 6,
        "category": "Surfing",
        "type": "generic"
    },
    "Other Activities High intensity" :{
        "masterActivityID" : 58,
        "METsValue": 6,
        "category": "Others",
        "type": "generic"
    },
    "Other Activities" :{
        "masterActivityID" : 59,
        "METsValue": 3,
        "category": "Others",
        "type": "generic"
    },
    "Walking treadmill" :{
        "masterActivityID" : 60,
        "METsValue": 4,
        "category": "Others",
        "type": "generic"
    },
    "Running Treadmill" :{
        "masterActivityID" : 61,
        "METsValue": 6.5,
        "category": "Others",
        "type": "generic"
    },
}
module.exports = activityMasterData 