const constants = {
    DATE_FORMAT : "DD-MM-YYYY",
    DATE_TIME_FORMAT :"DD-MM-YYYY hh:mm:ss",
    USER_ID_KEY: "UserID",
    DATE_KEY : "Date",
    DAILY_FIT_SCORE_KEY: "DailyFitScore",
    ACTIVITY_ID_KEY: "ActivityID",
    START_DATE_KEY: "StartDate",
    ACTIVITY_ID_DATE_TIME_FORMAT : "DDMMYYYYhhmmss",
    WEEKLY_FIT_SCORE_KEY :"WeeklyFitScore",
    BLAH : "ertyu" 
}

module.exports = constants

