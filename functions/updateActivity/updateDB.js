const moment = require("moment");
const _ = require("lodash")
const { DATE_KEY, DATE_FORMAT,ACTIVITY_ID_KEY,ACTIVITY_ID_DATE_TIME_FORMAT, USER_ID_KEY } = require("../constants");

const updateDB = async function (db, updateCollectionRequest){

    let userID = updateCollectionRequest.userID
    let isUserExist = updateCollectionRequest.isUserExist
    let primarySourceID = updateCollectionRequest.PrimarySourceID
    let primarySourceName = updateCollectionRequest.PrimarySourceName
    let token = updateCollectionRequest.token
    let totalNumberOfActivities = updateCollectionRequest.totalNumberOfActivities
    let apiActivities = updateCollectionRequest.apiActivities
    console.log("updateCollectionRequest ", updateCollectionRequest)

    let calendarRef = db.collection("calendar")

    apiActivities.forEach(element => {
        console.log("activity_id : ", element.ActivityID)
        console.log("activity_name", element.ActivityName)
        console.log("activity_duration", element.Duration)
     });

    await updateActivityCollection(db, apiActivities, userID)

    await updateCalendarCollection(db, apiActivities, userID)

     //Calculate weekly fit score 
     let weeklyCalendarData = await calendarRef.where(DATE_KEY,">=", moment().startOf('week').format(DATE_FORMAT)).where(USER_ID_KEY,"==",userID).get();
     let weeklyFitScore = 0 ;  
     weeklyCalendarData.forEach(doc => {
         weeklyFitScore += Number(doc.data().DailyFitScore) ;
     });
  
    await updateUserCollection(db, isUserExist, userID, primarySourceID, primarySourceName, totalNumberOfActivities, token, weeklyFitScore)
    
}

const updateUserCollection = async function (db, isUserExist, userID, primarySourceID, primarySourceName, totalNumberOfActivities, token, weeklyFitScore){
    console.log("inside updateUserCollection")
    let userRef = db.collection("users");
    try{
        if (!isUserExist) {
            await userRef.doc(userID.toString()).set({
                UserID: userID,
                PrimarySourceID: primarySourceID, 
                PrimarySourceName: primarySourceName,
                LastActivityUpdated: moment().utcOffset("+05:30").valueOf(),
                AccessToken: token,
                TotalNumberOfActivities: Number(totalNumberOfActivities),
                WeeklyFitScore: weeklyFitScore
            });
        } else {
            await userRef.doc(userID.toString()).update({
                LastActivityUpdated : moment().utcOffset("+05:30").valueOf(),
                TotalNumberOfActivities: Math.round(Number(totalNumberOfActivities)),
                WeeklyFitScore: weeklyFitScore,
                AccessToken: token
            });
        }
    } catch (e) {
        console.log("Exception in Updating User Collection", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }
    callback = {
                data: "Data updated successfully",
                status: 200,
                result: true
            }
            return callback
}

const updateCalendarCollection = async function (db, apiActivities, userID){
          //Update Calendar Collection
          console.log("inside updateCalenderCollection")
          let calendarRef = db.collection("calendar")
          let activityByDate = []
          let calendarToSet = []

        activityByDate = _.groupBy(apiActivities, function (o) {
            return moment(o.StartDate).utcOffset("+05:30").format(DATE_FORMAT)
        })
        for (let key in activityByDate) {
            let obj = {
                Date : key,                
                UserID: userID
            }
            let dailyFitScore = 0
            let activityIDs = []
                for (let i=0;i<activityByDate[key].length;i++) {
                    
                    if(!isNaN(activityByDate[key][i].ActivityScore)){
                        // for All other activity
                        dailyFitScore += activityByDate[key][i].ActivityScore 
                    }
                    else {
                        //For Daily Steps activity
                        //Daily Steps are not included
                    }
                    activityIDs.push(activityByDate[key][i].ActivityID)
                }
            obj.DailyFitScore = Number(dailyFitScore).toFixed(2)
            obj.ActivityIDs = activityIDs
            calendarToSet.push(obj)
        }
      //Set calendar 
      try{
        for (let i =0;i<calendarToSet.length;i++) {
            await calendarRef.doc(calendarToSet[i].Date+"_"+calendarToSet[i].UserID).set({
                Date: calendarToSet[i].Date,
                UserID: calendarToSet[i].UserID,
                ActivityIDs: calendarToSet[i].ActivityIDs,
                DailyFitScore: Number(calendarToSet[i].DailyFitScore)
            });            
        }
        
    } catch (e) {
        console.log("Exception in Updating Calendar collection : ", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }
    }

    //Update Activity Collection
const updateActivityCollection = async function (db, apiActivities, userID){
    console.log("inside updateActivityCollection ", apiActivities)
    console.log("inside updateActivityCollection userID ", userID)
    let activitiesRef = db.collection("activities")
    try{  for (let i=0;i<apiActivities.length;i++) {
                
        await activitiesRef.doc(apiActivities[i].ActivityID).set({
            UserID: userID, 
            ActivityName: apiActivities[i].ActivityName, 
            ActivityID: apiActivities[i].ActivityID, 
            StartDate: apiActivities[i].StartDate ? apiActivities[i].StartDate : undefined,
            EndDate: apiActivities[i].EndDate ? apiActivities[i].EndDate : undefined,
            Duration: apiActivities[i].Duration ? apiActivities[i].Duration : 0,
            CaloriesBurned: apiActivities[i].CaloriesBurned ? Math.round(Number(apiActivities[i].CaloriesBurned)) : 0,
            ActivityScore: apiActivities[i].ActivityScore ? Math.round(Number(apiActivities[i].ActivityScore)) : 0,
            StepCount: apiActivities[i].StepCount ? apiActivities[i].StepCount: 0,
            StepFitPoints: apiActivities[i].StepFitPoints ? Math.round(Number(apiActivities[i].StepFitPoints)) : 0,
            Distance: apiActivities[i].Distance ? Math.round(Number(apiActivities[i].Distance)) : 0,
            AveragePace: apiActivities[i].AveragePace ? Math.round(Number(apiActivities[i].AveragePace)) : 0,
            Source : apiActivities[i].Source
            });
        }
    } catch (e) {
        console.log("Exception in updating Activities", e)
        callback = {
            data : e,
            status: 402,
            result: false
        }
    }
    }

    module.exports = { updateDB }