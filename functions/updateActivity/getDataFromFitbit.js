const moment = require("moment");
const axios = require("axios");
const _ = require("lodash")
const activityMasterData = require("../activityMappers/activityMasterData");
const fitbitMapper = require("../activityMappers/fitbitMapping");
const { DATE_KEY, DATE_FORMAT,ACTIVITY_ID_KEY,ACTIVITY_ID_DATE_TIME_FORMAT } = require("../constants");

const getDataFromFitbit = async function(updateAPIRequest){
    // update fitbit data
    let token = updateAPIRequest.token
    let apiStartDate = updateAPIRequest.apiStartDate
    let apiEndDate = updateAPIRequest.apiEndDate
    let userID = updateAPIRequest.userID
    let lastActivityUpdated = updateAPIRequest.lastActivityUpdated
    let totalNumberOfActivities = updateAPIRequest.totalNumberOfActivities
    let activitiesRef = updateAPIRequest.activitiesRef
    let userExists = false;
    let sourceUserId = updateAPIRequest.sourceUserId
    let userDetails = updateAPIRequest.userDetails
    let apiActivities = []
    // console.log("Fitbit handler is called with request :", updateAPIRequest)
    if(!userDetails.empty) {
        userExists = true;
    }
    let currentDate = apiStartDate
    while (!moment(currentDate).isSameOrAfter(moment(apiEndDate))) {
        console.log("Current Date ", moment(currentDate))
        console.log("End Date ", moment(apiEndDate))
        await axios.get("https://api.fitbit.com/1/user/"+ sourceUserId + "/activities/date/" + moment(currentDate).format('YYYY-MM-DD') + ".json", {
            headers: {
                Authorization: token
            }
        })
        .then(async function(response) {
            let result = response.data
            console.log("Result Step data", result)
            if (result) {
                let obj = {}
                let activityID = userID + "_" +  moment(Number(currentDate)).utcOffset("+05:30").format(ACTIVITY_ID_DATE_TIME_FORMAT)
                obj.ActivityID = activityID
                obj.ActivityName = "Daily Steps"
                obj.StartDate = moment(Number(currentDate)).startOf('day').valueOf()
                obj.EndDate = moment(Number(currentDate)).add(1, "days").startOf('day').valueOf()
                obj.StepCount = result.summary.steps
                obj.StepFitPoints = calculateFitBitStepFitPoints(obj.StepCount)
                obj.Source = "Fitbit"
                if(lastActivityUpdated && moment(obj.StartDate).utcOffset("+05:30").isSameOrBefore(lastActivityUpdated)){
                    let exisingActivity = await activitiesRef.where(ACTIVITY_ID_KEY,"==", activityID).get();
                    if(exisingActivity && exisingActivity.length > 0){
                       // if already exist do not increase activity count
                    } else{                        
                        totalNumberOfActivities ++ 
                    }
                } else {
                    totalNumberOfActivities ++
                }
                apiActivities.push(obj)
            }
        })
        .catch(async function (error) {
            console.log("Error Excpetion api 1 ", error)
            throw error;
        })
        currentDate = moment(currentDate).add(1,"days").valueOf()
    }
    let requestUrl = 'https://api.fitbit.com/1/user/' + sourceUserId + '/activities/list.json?';
//         let requestUrl = 'https://api.fitbit.com/1/user/' + "-";
    requestUrl = requestUrl + 'beforeDate=' + moment(apiEndDate).format('YYYY-MM-DD') + '&afterDate=' + moment(apiStartDate).format('YYYY-MM-DD');
    requestUrl = requestUrl + '&sort=desc&limit=20&offset=0';
    
    await axios.get(requestUrl,{
        headers: {
            Authorization:token
        }
    })
    .then(async function (response){
        let result = response.data
        console.log("Result ", result)
        if (result.activities && result.activities.length > 0) {
            for (let i=0;i<result.activities.length;i++) {
                let obj = {}
                //ActivityID will be combination of UserID and timestamp
                let activityID = userID + "_" +  moment(result.activities[i].originalStartTime).utcOffset("+05:30").format(ACTIVITY_ID_DATE_TIME_FORMAT)
                obj.ActivityID = activityID
                let activityName =  fitbitMapper[result.activities[i].activityTypeId]
                console.log("---------------------------")
                console.log("UserActivity Name master", activityName)
                console.log("---------------------------")
                let activityConfig = activityMasterData[activityName]
                if(activityName && activityConfig && result.activities[i].logType !== "manual") {
                    let metValue = activityConfig.METsValue;
                    obj.ActivityName = activityName
                    obj.StartDate = moment(result.activities[i].startTime).valueOf()
                    let activityDuration = result.activities[i].duration
                    obj.Duration = result.activities[i].activeDuration/60000
                    obj.EndDate = obj.StartDate + obj.Duration
                    obj.CaloriesBurned = result.activities[i].calories
                    obj.HeartPoints = result.activities[i].averageHeartRate
                    if (obj.Duration) {
                        if (obj.HeartPoints) {
                            obj.ActivityScore = calculateFitBitActivityScore(obj.HeartPoints, obj.Duration, metValue)
                        } else {
                            obj.ActivityScore = calculateFitBitActivityScore(0, obj.Duration, metValue)
                        }
                    }
                    obj.StepCount = result.activities[i].steps
                    obj.Distance = result.activities[i].distance
                    obj.AveragePace = result.activities[i].speed
                    obj.Source = "Fitbit"
                    obj.StepFitPoints = calculateFitBitStepFitPoints(obj.StepCount)
                    obj.StartDate = moment(result.activities[i].startTime, "YYYY-MM-DD").startOf('day').format("MM-DD-YYYY hh:mm:ss")
                    if(isMovementActivity(obj)){
                        obj.ActivityScore = obj.AveragePace < 8.50 ? ( 2 * obj.Duration) : obj.Duration
                    }
                    if(!obj.ActivityScore){
                        obj.ActivityScore = calculateFitBitActivityScore(0, activityDuration, metValue)
                    } 
                    if(lastActivityUpdated && moment(obj.StartDate).utcOffset("+05:30").isSameOrBefore(lastActivityUpdated)) {
                        let exisingActivity = await activitiesRef.where("ActivityID","==", activityID).get();
                        if(exisingActivity && exisingActivity.length > 0){
                             // if already exist do not increase activity count
                        } else{                        
                        totalNumberOfActivities++ 
                        }
                    } else { 
                    totalNumberOfActivities++ 
                    }

                    //Check if it is movement activity
                    if(isMovementActivity(obj)) {

                       if(obj.Distance >= 500){
                        apiActivities.push(obj)
                       } else {
                        totalNumberOfActivities -- 
                       }
                    } else {
                        apiActivities.push(obj)
                    }
                    
                } else {
                    console.log("Invalid activity")
                }
            }
        } else {
            console.log("No data")
        }
        
    })
    .catch(function (error){
        console.log("Error Excpetion api 2 ", error)
        throw error;
    })
    let apiResponse = {
        success: true,
        status : 200,
        apiActivities : apiActivities,
        totalNumberOfActivities : totalNumberOfActivities
    }
    return  apiResponse;
}
const calculateFitBitStepFitPoints = function(steps){
    return Math.round(steps * 0.002)
}

const calculateFitBitActivityScore = function (heartPoints, duration, metValue) {
    
    if (heartPoints > duration) {
        return heartPoints
    } else {
        if(metValue<6){
            return duration
        } else {
            return  2*duration
        }
    }
}


const isMovementActivity = function (activity){
    return activity.ActivityName == "Walking" || activity.ActivityName == "Running"
}

module.exports = { getDataFromFitbit }


//62D3YZ