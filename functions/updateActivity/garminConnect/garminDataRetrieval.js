var garminAuth = require('./garminAuth');
var config = require('./config');
const axios = require('axios');
const crypto = require('crypto');
const oauth1a = require('oauth-1.0a');


const CONSUMERKEY = '214da833-68ff-4b5a-ae1b-b90f00e666eb';
const CONSUMERSECRET = 'rmTFsvXxLwbLQi4jG1rxs8YUCgq8Cjq39Cj';

class garminDataRetrieval{

    init(){

    }
    async getUserToken(input){
        try{
            var response = await this.performAppAuthentication(input);
            var userRequestData = await this.generateUserRequest(response);
            return userRequestData;
        }catch(e){
            console.log("dataRetrieval ", e);
        }
    }

    async updateGarminData(){
        try{
            var data = "this function will fetch garmin data"
            return data;
        }catch(e){
            console.log("dataRetrieval ", e);
        }
    }

    async performAppAuthentication(input){
        try{
            var inputURL;
            if (input['action'] === 'appValidation'){
                console.log("inside app validator")
                const request = {
                    url: 'https://connectapi.garmin.com/oauth-service/oauth/request_token',
                    method: 'POST',
                    body: {}
                };
                var finalHeader = await this.getAppHeader(request);
                var responseToken = await axios.post(request.url,request.body,{ headers: finalHeader }).then((response)=>{
                    console.log("Pringting responseData")
                    console.log(response)
                    return {"appAuthToken" : response.data.split('&')[0].split("=")[1],"appAuthTokenSecret" : response.data.split('&')[1].split("=")[1]};
                });
                return responseToken;
            }else if(input['action'] === 'userDataAccess') {
                inputURL = 'https://connectapi.garmin.com/oauth-service/oauth/access_token'
                let newToken = input['token'].split('&')[0]
                let verifier = input['token'].split('&')[1]
                let userTokenData = garminAuth.get_oAuthToken_Signature_UserAccess("GET","https://connect.garmin.com/oauthConfirm",CONSUMERKEY,CONSUMERSECRET,newToken,input['token_secret'],verifier).then((auth_header)=>{
                garminAuth.get_oAuthToken_UserAccessFromGarmin(inputURL,auth_header).then((data)=>{
                    return {"userAccessToken" : data.split('&')[0].split("=")[1],"userAccessSecret" : data.split('&')[q].split("=")[1]};
                });
            });
            return userTokenData;
            }
        }catch(e){
            console.log("performAppAuthentication ", e);
        }
    }
    async generateUserRequest(token){
        try{
            const userRequest = {
                url: 'https://connect.garmin.com/oauthConfirm?oauth_token='+token.appAuthToken,
                method: 'GET',
                token_secret : token.appAuthTokenSecret
            }
            return userRequest;
        }catch(e){
            console.log("performAppAuthentication ", e);
        }
    }
    async getAppHeader(request,token){
        const oauth = oauth1a({
            consumer: { key: CONSUMERKEY, secret: CONSUMERSECRET },
            signature_method: 'HMAC-SHA1',
            hash_function(base_string, key) {
                return crypto.createHmac('sha1', key).update(base_string).digest('base64')},
        })
        const authorization = oauth.authorize(request,token)
        return oauth.toHeader(authorization);
    };

}

module.exports = new garminDataRetrieval;