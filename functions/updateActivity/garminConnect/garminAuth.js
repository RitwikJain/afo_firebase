var oAuthSig = require('oauth-signature');
const axios = require('axios');
var oAuthFunctions_all_debugThis = false;
var oAuthFunctionNames_all_debugThis = false || oAuthFunctions_all_debugThis;

const c_oauth_version = '1.0';

const c_signatureMethod = 'HMAC-SHA1';


const parameter_separator = ',';


getOAuthHeader = (params, callerDebug) => {
    var msgPrefix = 'getOAuthHeader';
    var debugThis = false || oAuthFunctions_all_debugThis;

    if (debugThis || callerDebug) {
        console.log(msgPrefix, 'params:', JSON.stringify(params));
    }

    var header = 'OAuth ';
    var end = params.length;

    for (var i = 0; i < end; i++) {
        if (params[i].key.indexOf('oauth_') !== 0) continue;
        let p = params[i].key + '="' + params[i].value + '"' + (i < end - 1 ? parameter_separator : '');
        header += p;
    }


    if (debugThis || callerDebug) {
    console.log(msgPrefix, 'returning:', header);
    }
    return header;
};


getEpochTimeInSeconds = () => {
    var epochInSeconds = Math.round(new Date().getTime() / 1000);
    return epochInSeconds;
};


const get_oAuthToken_SignatureInformation = async (httpMethod, url, consumerKey, consumerPrivate) => {

    var debugThis = false || oAuthFunctions_all_debugThis;
    if (oAuthFunctionNames_all_debugThis || debugThis) {
        console.log('get_oAuthToken_SignatureInformation ');
    }


    let text = ''
    const possible = '0123456789'
    for (let i = 0; i < 11; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    // var nonce = random.genToken_Nonce();
    var nonce = text;
    var timestamp = getEpochTimeInSeconds();
    var signatureMethod = c_signatureMethod;
    var oauth_version = c_oauth_version;

    var parameters = {
        oauth_consumer_key: consumerKey,
        oauth_signature_method: signatureMethod,
        oauth_nonce: nonce,
        oauth_timestamp: timestamp,
        oauth_version: oauth_version,
    };

    if (debugThis) {
        console.log('get_oAuthToken_SignatureInformation INPUT parameters ' + JSON.stringify(parameters));
    }


    let signature = oAuthSig.generate(httpMethod, url, parameters, consumerPrivate);


    var retval = {
        consumerKey: consumerKey,
        nonce: nonce,
        timestamp: timestamp.toString(),
        signature: signature,
        signatureMethod: signatureMethod,
        oauth_version: oauth_version,
    };


    if (debugThis) {
        console.log('get_oAuthToken_SignatureInformation OUTPUT ' + JSON.stringify(retval));
    }
    return retval;
};



const get_oAuthToken_Signature_UserAccess = async (httpMethod, url, consumerKey, consumerPrivate, token, tokenPrivate, verifier) => {
    var debugThis = false || oAuthFunctions_all_debugThis;

    if (oAuthFunctionNames_all_debugThis || debugThis) {
        console.log('get_oAuthToken_Signature_UserAccess ');
    }


    // var nonce = random.genToken_Nonce();
    let text = ''
    const possible = '0123456789'
    for (let i = 0; i < 11; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    var nonce = text;
    var timestamp = getEpochTimeInSeconds();
    var signatureMethod = c_signatureMethod;
    var oauth_version = c_oauth_version;

    var parameters = {
        oauth_consumer_key: consumerKey,
        oauth_nonce: nonce,
        oauth_signature_method: signatureMethod,
        oauth_timestamp: timestamp,
        oauth_token: token,
        oauth_verifier: verifier,
        oauth_version: oauth_version,
    };


    if (oAuthFunctionNames_all_debugThis || debugThis) {
        console.log('get_oAuthToken_Signature_UserAccess INPUT parameters ' + JSON.stringify(parameters));
    }


    let signature = oAuthSig.generate(httpMethod, url, parameters, consumerPrivate, tokenPrivate);

    var retval = {
        consumerKey: consumerKey,
        nonce: nonce,
        timestamp: timestamp.toString(),
        signature: signature,
        signatureMethod: signatureMethod,
        oauth_version: oauth_version,
        token: token,
        verifier: verifier,
    };

    if (debugThis) {
        console.log('get_oAuthToken_Signature_UserAccess OUTPUT ' + JSON.stringify(retval));
    }
    return retval;
    };


    // this call will redirect you to garmin's web site for login and permission

const get_oAuthToken_UserAccessFromGarmin = async (url, oAuth_UserAccess_SignatureInfo) => {
        var debugThis = false || oAuthFunctions_all_debugThis;
        
        if (oAuthFunctionNames_all_debugThis || debugThis) {
            console.log('get_oAuthToken_UserAccessFromGarmin ');
    }


    var parameters = [];
    parameters.push({key: 'oauth_nonce', value: oAuth_UserAccess_SignatureInfo.nonce});
    parameters.push({key: 'oauth_signature', value: oAuth_UserAccess_SignatureInfo.signature});
    parameters.push({key: 'oauth_consumer_key', value: oAuth_UserAccess_SignatureInfo.consumerKey});
    parameters.push({key: 'oauth_timestamp', value: oAuth_UserAccess_SignatureInfo.timestamp});
    parameters.push({key: 'oauth_signature_method', value: oAuth_UserAccess_SignatureInfo.signatureMethod});
    parameters.push({key: 'oauth_token', value: oAuth_UserAccess_SignatureInfo.token});
    parameters.push({key: 'oauth_verifier', value: oAuth_UserAccess_SignatureInfo.verifier});
    parameters.push({key: 'oauth_version', value: oAuth_UserAccess_SignatureInfo.oauth_version});


    const oAuth = getOAuthHeader(parameters, debugThis);
    var config = {
    headers: {Authorization: oAuth},
    };


    if (debugThis) {
        console.log('get_oAuthToken_UserAccessFromGarmin headers', JSON.stringify(config));
    }

    var retval = {
        code: 200,
        message: '',
        tokens: null,
    };


    var response = null;
    try {
        let data = null;
        response = await axios.post(url, data, config);
    } catch (err) {
        retval.code = 400;
        (retval.message = 'ERROR get_oAuthToken_UserAccessFromGarmin catch'), JSON.stringify(err);
        console.log(JSON.stringify(retval));
        return retval;
    }

    if (response) {
        retval.tokens = response.data.slice("&");
        console.log(JSON.stringify(retval));
    } else {
        retval.code = 400;
        retval.message = 'ERROR get_oAuthToken_UserAccessFromGarmin axios call failure';
        console.log(JSON.stringify(retval));
        return retval;
    }

    retval.code = 200;
    retval.message = 'Success';
    if (debugThis) {
        console.log('get_oAuthToken_UserAccessFromGarmin returning', JSON.stringify(retval));
    }
    return retval;
};

module.exports = {get_oAuthToken_SignatureInformation,get_oAuthToken_Signature_UserAccess,get_oAuthToken_UserAccessFromGarmin,};